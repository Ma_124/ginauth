![Logo](https://gitlab.com/Ma_124/pages-static-shared/raw/master/ginauth2.svg)

# Ginauth
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/Ma_124/ginauth)](https://goreportcard.com/report/gitlab.com/Ma_124/ginauth)
[![CodeCov](https://codecov.io/gl/Ma_124/ginauth/branch/master/graph/badge.svg)](https://codecov.io/gl/Ma_124/ginauth)
[![Build Status](https://gitlab.com/Ma_124/ginauth/badges/master/build.svg)](https://gitlab.com/Ma_124/ginauth/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/Ma_124/ginauth?status.svg)](https://godoc.org/gitlab.com/Ma_124/ginauth)
[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://ma124.js.org/l/MIT/~/2019)

Package `ginauth` implements a [RFC 7235][rfc7235] compliant auth middleware

## Features
- [x] Basic  ([RFC 7617][rfc7617])
- [x] Bearer ([RFC 6750][rfc6750])
  - [x] Introspection ([RFC 7662][rfc7662])
  - [x] JWT           ([RFC 7519][rfc7519])

## Examples

```go
package yourproject

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/auth"
	"github.com/gin-gonic/gin"
	. "gitlab.com/Ma_124/ginauth"
	"gitlab.com/Ma_124/ginlog"
)

const OpenIDURL = "https://oauth2.example.org/.well-known/openid-configuration"
const IntrospectionEndpoint = "https://oauth2.example.org/token_info"

var basicUsers = map[string]string{
	"admin": "p4ssword",
	"alice": "qwerty",
	"bob": "123456",
}

func main() {
	r := gin.New()
	r.Use(ginlog.Logger())
	r.Use(gin.Recovery())

	authorized := r.Group("/internal")

	authorized.Use(Middleware(
		OrMap(map[string]AuthFunc{
			"basic": BasicAuth(basicUsers),
			"bearer": Or(
				BearerJWTRoleAuth(auth.NewJwtContext(OpenIDURL), "yourproject", "access"),
				BearerIntrospectionURLScopeAuth(IntrospectionEndpoint, "access"),
			),
		}),
	))
	// OR just:
	authorized.Use(Middleware(BearerJWTRoleAuth(auth.NewJwtContext(OpenIDURL), "yourproject", "access")))
}
```

## License

Copyright 2019 Ma_124, ma124.js.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

The icon of this project is made out of the [Gin-Gonic Framework Logo](https://github.com/gin-gonic/logo#license) by Javier Provecho is which is licensed under a  [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/) and the key icon was made by [Freepik](https://www.freepik.com/home) from www.flaticon.com

[rfc7235]: https://tools.ietf.org/html/rfc7235
[rfc7617]: https://tools.ietf.org/html/rfc7617
[rfc6750]: https://tools.ietf.org/html/rfc6750
[rfc7662]: https://tools.ietf.org/html/rfc7662
[rfc7519]: https://tools.ietf.org/html/rfc7519

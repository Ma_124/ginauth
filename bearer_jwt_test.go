// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var Handler *testHandler

func TestBearerJWTRoleAuth2(t *testing.T) {
	validToken, err := getTestToken(true)
	if err != nil {
		panic(err)
	}
	invalidToken, err := getTestToken(false)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, true, BearerJWTRoleAuth(Handler.JWTCtx, "cid", "rol")(nil, "bearer", validToken))
	assert.Equal(t, false, BearerJWTRoleAuth(Handler.JWTCtx, "cid", "nonexistent")(nil, "bearer", validToken))

	assert.Equal(t, false, BearerJWTFuncAuth(Handler.JWTCtx, "cid", nil)(nil, "basic", validToken))
	assert.Equal(t, false, BearerJWTFuncAuth(Handler.JWTCtx, "cid", nil)(nil, "bearer", invalidToken))
}

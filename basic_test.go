// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBasicAuth(t *testing.T) {
	tests := map[string]struct {
		Users  map[string]string
		Scheme string
		Tok    string
		Want   bool
	}{
		"wrong scheme": {
			Scheme: "bearer",
			Tok:    "YWRtaW46cHc=",
			Want:   false,
		},
		"base64 err": {
			Tok:  "YWRtaW4!6cHc=",
			Want: false,
		},
		"no pw": {
			Tok:  "YWRtaW4hcHc=",
			Want: false,
		},
		"unknown user": {
			Tok:  "YWRtaW4yOnB3",
			Want: false,
		},
		"wrong pw": {
			Tok:  "YWRtaW46cGFzc3dvcmQ=",
			Want: false,
		},
		"success": {
			Tok:  "YWRtaW46cHc=",
			Want: true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.Users == nil {
				tt.Users = map[string]string{"admin": "pw"}
			}

			if tt.Scheme == "" {
				tt.Scheme = "basic"
			}

			got := BasicAuth(tt.Users)(&gin.Context{}, tt.Scheme, tt.Tok)
			assert.Equal(t, tt.Want, got)
		})
	}
}

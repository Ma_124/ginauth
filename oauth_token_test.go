// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"strconv"
	"time"
)

func getTestToken(stillValid bool) (tok string, err error) {
	var exp string
	if stillValid {
		exp = strconv.Itoa(int(time.Now().Unix()) + int((10 * time.Minute).Seconds()))
	} else {
		exp = "1560793964"
	}

	headerAndPayload := `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkhwcFV1WkxsdzlpT0pETndwQXR2cndJMk02Qm91UW9qQ0VyNVB5SFRLVXMifQ.` + // Constant header
		base64.RawURLEncoding.EncodeToString([]byte(`{"jti":"988efd2e-af74-4671-9997-b69a23df4cd6","exp":`+exp+ // Payload; expires at
			`,"nbf":0,"iat":1560778713,"iss":"`+Handler.URL+"/oidc"+ // Issuer
			`","aud":["cid","account"],"sub":"f0019936-74e5-467e-a047-80c422dc5f3d","typ":"Bearer","azp":"cid","auth_time":0,"session_state":"cf5515c4-0607-4f47-9219-bc8f2cb4e113","acr":"1","realm_access":{"roles":["offline_access","uma_authorization"]},"resource_access":{"cid":{"roles":["rol"]},"account":{"roles":["manage-account","manage-account-links","view-profile"]}},"scope":"profile email","email_verified":false,"preferred_username":"test-user"}`))

	checksum := sha256.New()
	checksum.Write([]byte(headerAndPayload))

	signature, err := rsa.SignPKCS1v15(rand.Reader, Handler.PrivKey, crypto.SHA256, checksum.Sum(nil))
	if err != nil {
		return
	}

	return headerAndPayload + "." + base64.RawURLEncoding.EncodeToString(signature), nil
}

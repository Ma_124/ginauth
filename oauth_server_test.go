// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/auth"
	"context"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strings"
)

type testHandler struct {
	URL     string
	JWKS    *JWKS
	PubKey  *rsa.PublicKey
	PrivKey *rsa.PrivateKey
	JWTCtx  *auth.JwtContext
}

type JWKS struct {
	Keys []*JWK `json:"keys"`
}

type JWK struct {
	Algorithm string `json:"alg,omitempty"`
	KeyType   string `json:"kty,omitempty"`
	Use       string `json:"use,omitempty"`
	ID        string `json:"kid,omitempty"`

	KeyModulus  string `json:"n,omitempty"`
	KeyExponent string `json:"e,omitempty"`

	X509CertChain []string `json:"x5c,omitempty"`
	X509CertThumb string   `json:"x5t,omitempty"`
}

func (h *testHandler) init() {
	data, err := ioutil.ReadFile("testdata/id_pub.pem")
	if err != nil {
		panic(err)
	}

	p, rest := pem.Decode(data)
	if p == nil || len(rest) == len(data) {
		panic("failed to decode PEM")
	}

	intPubKey, err := x509.ParsePKIXPublicKey(p.Bytes)
	if err != nil {
		panic(err)
	}

	pubKey := intPubKey.(*rsa.PublicKey)

	data, err = ioutil.ReadFile("testdata/id.pem")
	if err != nil {
		panic(err)
	}

	p, rest = pem.Decode(data)
	if p == nil || len(rest) == len(data) {
		panic("failed to decode PEM")
	}

	privKey, err := x509.ParsePKCS1PrivateKey(p.Bytes)
	if err != nil {
		panic(err)
	}

	h.PrivKey = privKey
	h.PubKey = pubKey
	h.JWKS = &JWKS{
		[]*JWK{
			{
				Algorithm: "RS256",
				KeyType:   "RSA",
				Use:       "sig",
				ID:        "HppUuZLlw9iOJDNwpAtvrwI2M6BouQojCEr5PyHTKUs",

				KeyExponent: "AQAB",
				KeyModulus:  "1ZWR0VrTCfC4iITHK7v0m5dfVaaoht37RBq2lf68pfpXfCgFTK7KD5WdMcpqkussWiklXkQUbHPWa3JxgZ8xLkuAgBQfNms_ASm1TYaaLNv29Ke9b3MTAEiFAnfJJdV99L-wmvvGO-7HKW50g63h2k4vrcwdGRvLwpFojgEggorQahY8d-ojvVXsfM7LRH36Ve-BvpRLY0RwQN0W35BMFfSPCDjtSE6jhjs1RRmX2iVGsI_vn0l-EJZ-rtMFaAE4RMIxV2UKZHqY83OWO7qFQVvG5ubas4ySs1juh-zv2DhOb3Vy_Z7TtkLHCx7XGTPatKsMmFpeNbFZpYU_NxxXqw",
			},
		},
	}

	h.JWTCtx = auth.NewJwtContext(h.URL + "/oidc")

	Handler = h
}

func (h *testHandler) ServeHTTP(respw http.ResponseWriter, req *http.Request) {
	req.URL.Path = strings.TrimPrefix(strings.TrimSuffix(req.URL.Path, "/"), "/")

	var resp interface{}

	respw.Header().Set("Content-Type", "application/json")

urlSwitch:
	switch req.URL.Path {
	case "oidc/certs":
		resp = h.JWKS
	case "oidc/.well-known/openid-configuration":
		resp = gin.H{
			"issuer":   h.URL + "/oidc",
			"jwks_uri": h.URL + "/oidc/certs",
		}
	case "introspection":
		err := req.ParseForm()
		if err != nil {
			respw.WriteHeader(400)
			break
		}

		tok := req.FormValue("token")
		switch tok {
		case "fail with code":
			respw.WriteHeader(400)
			break urlSwitch
		case "invalid json":
			respw.WriteHeader(200)
			_, _ = respw.Write([]byte("a"))
			break urlSwitch
		default:
			token, err := h.JWTCtx.VerifyToken(context.Background(), tok, "cid")
			if err != nil {
				resp = OAuth2TokenIntrospection{
					Active: false,
				}
				break
			}

			resp = OAuth2TokenIntrospection{
				Active:    true,
				Scope:     "start middle end",
				ClientID:  "cid",
				Username:  "test-user",
				ExpiresAt: token.Expiry.Unix(),
			}
		}
	default:
		respw.WriteHeader(404)
		data, err := json.Marshal(gin.H{"error": "not found"})
		if err != nil {
			panic(err)
		}
		_, _ = respw.Write(data)
		return
	}

	respw.WriteHeader(200)
	data, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}
	_, _ = respw.Write(data)
}

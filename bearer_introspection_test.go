// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/url"
	"strings"
	"testing"
)

func TestBearerIntrospectionAuth(t *testing.T) {
	validToken, err := getTestToken(true)
	if err != nil {
		panic(err)
	}
	invalidToken, err := getTestToken(false)
	if err != nil {
		panic(err)
	}

	newReq := func(c *gin.Context, tok string, form url.Values) (*http.Request, error) {
		req, err := http.NewRequest("POST", Handler.URL+"/introspection", strings.NewReader(form.Encode()))
		if err != nil {
			return nil, err
		}
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		return req, nil
	}

	assert.Equal(t, false, BearerIntrospectionReqFuncFuncAuth(func(c *gin.Context, tok string, form url.Values) (*http.Request, error) {
		return nil, errors.New("error")
	}, nil)(nil, "bearer", validToken))

	assert.Equal(t, false, BearerIntrospectionURLFuncAuth(Handler.URL+"/introspection", nil)(nil, "basic", validToken))

	assert.Equal(t, false, BearerIntrospectionURLFuncAuth(Handler.URL+"/introspection", nil)(nil, "bearer", invalidToken))
	assert.Equal(t, false, BearerIntrospectionURLFuncAuth(Handler.URL+"/introspection", nil)(nil, "bearer", "fail with code"))
	assert.Equal(t, false, BearerIntrospectionURLFuncAuth(Handler.URL+"/introspection", nil)(nil, "bearer", "invalid json"))

	assert.Equal(t, true, BearerIntrospectionURLScopeAuth(Handler.URL+"/introspection", "start")(nil, "bearer", validToken))
	assert.Equal(t, true, BearerIntrospectionURLScopeAuth(Handler.URL+"/introspection", "middle")(nil, "bearer", validToken))
	assert.Equal(t, true, BearerIntrospectionURLScopeAuth(Handler.URL+"/introspection", "end")(nil, "bearer", validToken))

	assert.Equal(t, true, BearerIntrospectionReqFuncScopeAuth(newReq, "start")(nil, "bearer", validToken))
	assert.Equal(t, true, BearerIntrospectionReqFuncScopeAuth(newReq, "middle")(nil, "bearer", validToken))
	assert.Equal(t, true, BearerIntrospectionReqFuncScopeAuth(newReq, "end")(nil, "bearer", validToken))

	assert.Equal(t, false, BearerIntrospectionURLScopeAuth(Handler.URL+"/introspection", "nonexistent")(nil, "bearer", validToken))
	assert.Equal(t, false, BearerIntrospectionReqFuncScopeAuth(newReq, "nonexistent")(nil, "bearer", validToken))
}

// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"time"
)

// HTTPClient is the http.Client that is used for all http request by this package
// It provides a default timeout of 10 seconds.
var HTTPClient = &http.Client{
	Timeout: 10 * time.Second,
}

// Middleware returns a middleware which aborts with the status 401 if the given AuthFunc fails
func Middleware(authorizer AuthFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		ps := strings.SplitN(c.GetHeader("Authorization"), " ", 2)
		var tok string
		if len(ps) > 1 {
			tok = ps[1]
		}
		authed := authorizer(c, strings.ToLower(ps[0]), tok)
		if !authed {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		c.Next()
	}
}

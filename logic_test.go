// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TrueAuth(c *gin.Context, scheme string, tok string) bool {
	return true
}

func FalseAuth(c *gin.Context, scheme string, tok string) bool {
	return false
}

func TestAndOr(t *testing.T) {
	tests := map[string]struct {
		Authorizers []AuthFunc
		And         bool
		Or          bool
	}{
		"000": {
			Authorizers: []AuthFunc{FalseAuth, FalseAuth, FalseAuth},
			And:         false,
			Or:          false,
		},
		"111": {
			Authorizers: []AuthFunc{TrueAuth, TrueAuth, TrueAuth},
			And:         true,
			Or:          true,
		},
		"11": {
			Authorizers: []AuthFunc{TrueAuth, TrueAuth, TrueAuth},
			And:         true,
			Or:          true,
		},
		"1": {
			Authorizers: []AuthFunc{TrueAuth, TrueAuth, TrueAuth},
			And:         true,
			Or:          true,
		},
		"001": {
			Authorizers: []AuthFunc{FalseAuth, FalseAuth, TrueAuth},
			And:         false,
			Or:          true,
		},
		"100": {
			Authorizers: []AuthFunc{TrueAuth, FalseAuth, FalseAuth},
			And:         false,
			Or:          true,
		},
		"101": {
			Authorizers: []AuthFunc{TrueAuth, FalseAuth, TrueAuth},
			And:         false,
			Or:          true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			assert.Equal(t, tt.And, And(tt.Authorizers...)(nil, "", ""))
			assert.Equal(t, tt.Or, Or(tt.Authorizers...)(nil, "", ""))
		})
	}
}

func TestOrMap(t *testing.T) {
	authorizers := map[string]AuthFunc{
		"basic": BasicAuth(map[string]string{
			"u1": "p1", // dTE6cDE=
			"u2": "p2", // dTI6cDI=
		}),
	}

	tests := []struct {
		name   string
		scheme string
		tok    string
		want   bool
	}{
		{
			name:   "basic valid",
			scheme: "basic",
			tok:    "dTE6cDE=",
			want:   true,
		},
		{
			name:   "basic invalid",
			scheme: "basic",
			tok:    "dTM6cDE=", // u3:p1
			want:   false,
		},
		{
			name:   "nonexistent",
			scheme: "nonexistent",
			tok:    "tok",
			want:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, OrMap(authorizers)(nil, tt.scheme, tt.tok))
		})
	}
}

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)
	sh := &testHandler{}
	s := httptest.NewServer(sh)
	defer s.Close()
	sh.URL = s.URL
	sh.init()

	m.Run()
}

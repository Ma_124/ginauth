// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"github.com/gin-gonic/gin"
)

// An AuthFunc returns true if a request with the given authorization scheme and token should be allowed
type AuthFunc func(c *gin.Context, scheme string, tok string) bool

// And returns an AuthFunc which returns true if all authorizers returned true
func And(authorizers ...AuthFunc) AuthFunc {
	return func(c *gin.Context, scheme string, tok string) bool {
		for _, auth := range authorizers {
			if !auth(c, scheme, tok) {
				return false
			}
		}

		return true
	}
}

// Or returns an AuthFunc which returns true if at least one authorizer returned true
func Or(authorizers ...AuthFunc) AuthFunc {
	return func(c *gin.Context, scheme string, tok string) bool {
		for _, auth := range authorizers {
			if auth(c, scheme, tok) {
				return true
			}
		}

		return false
	}
}

// OrMap returns an AuthFunc which looks authorizer up by scheme and passes the arguments to it
func OrMap(authorizers map[string]AuthFunc) AuthFunc {
	return func(c *gin.Context, scheme string, tok string) bool {
		auth, ok := authorizers[scheme]
		if !ok {
			return false
		}

		return auth(c, scheme, tok)
	}
}

// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

// OAuth2TokenIntrospection is a struct containing the fields specified in RFC 7662 Section 2.2
type OAuth2TokenIntrospection struct {
	Active bool `json:"active"`

	Scope     string `json:"scope,omitempty"`
	ClientID  string `json:"client_id,omitempty"`
	Username  string `json:"username,omitempty"`
	TokenType string `json:"token_type,omitempty"`
	ExpiresAt int64  `json:"exp,omitempty"`
	IssuedAt  int64  `json:"iat,omitempty"`
	NotBefore int64  `json:"nbf,omitempty"`
	Subject   string `json:"sub,omitempty"`
	Audience  string `json:"aud,omitempty"`
	Issuer    string `json:"iss,omitempty"`
	JWTID     string `json:"jti,omitempty"`
}

// BearerIntrospectionReqFuncFuncAuth implements Bearer Introspection (RFC 7662) Auth by creating a request to an introspection endpoint with the given function (newIntrospectionReq) and
// calling the other function (fn) with the OAuth2TokenIntrospection if the request was successful
func BearerIntrospectionReqFuncFuncAuth(newIntrospectionReq func(c *gin.Context, tok string, form url.Values) (*http.Request, error), fn func(tok *OAuth2TokenIntrospection) bool) AuthFunc {
	return func(c *gin.Context, scheme string, tok string) bool {
		if scheme != "bearer" {
			return false
		}

		form := url.Values{}
		form.Set("token", tok)

		req, err := newIntrospectionReq(c, tok, form)
		if err != nil {
			return false
		}

		resp, err := HTTPClient.Do(req)
		if err != nil {
			return false
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			return false
		}

		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return false
		}

		tokResp := &OAuth2TokenIntrospection{}

		err = json.Unmarshal(data, tokResp)
		if err != nil {
			return false
		}

		if !tokResp.Active {
			return false
		}

		return fn(tokResp)
	}
}

// BearerIntrospectionURLFuncAuth implements Bearer Introspection (RFC 7662) Auth by creating a request from the given URL and passing that to BearerIntrospectionReqFuncFuncAuth
func BearerIntrospectionURLFuncAuth(introspectionEndpoint string, fn func(tok *OAuth2TokenIntrospection) bool) AuthFunc {
	return BearerIntrospectionReqFuncFuncAuth(func(c *gin.Context, tok string, form url.Values) (*http.Request, error) {
		req, err := http.NewRequest("POST", introspectionEndpoint, strings.NewReader(form.Encode()))
		if err != nil {
			return nil, err
		}
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		return req, nil
	}, fn)
}

// BearerIntrospectionURLScopeAuth implements Bearer Introspection (RFC 7662) Auth by creating a request with BearerIntrospectionURLFuncAuth and checking whether the given scope is in the response
func BearerIntrospectionURLScopeAuth(introspectionEndpoint string, scope string) AuthFunc {
	return BearerIntrospectionURLFuncAuth(introspectionEndpoint, func(tok *OAuth2TokenIntrospection) bool {
		return strings.HasPrefix(tok.Scope, scope+" ") || strings.Contains(tok.Scope, " "+scope+" ") || strings.HasSuffix(tok.Scope, " "+scope)
	})
}

// BearerIntrospectionReqFuncScopeAuth implements Bearer Introspection (RFC 7662) Auth by calling BearerIntrospectionReqFuncFuncAuth and checking whether the given scope is in the response
func BearerIntrospectionReqFuncScopeAuth(newIntrospectionReq func(c *gin.Context, tok string, form url.Values) (*http.Request, error), scope string) AuthFunc {
	return BearerIntrospectionReqFuncFuncAuth(newIntrospectionReq, func(tok *OAuth2TokenIntrospection) bool {
		return strings.HasPrefix(tok.Scope, scope+" ") || strings.Contains(tok.Scope, " "+scope+" ") || strings.HasSuffix(tok.Scope, " "+scope)
	})
}

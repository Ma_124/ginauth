// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"encoding/base64"
	"github.com/gin-gonic/gin"
	"strings"
)

// BasicAuth implements Basic (RFC 7617) Auth by looking the user up in the given map and matching their password
func BasicAuth(users map[string]string) AuthFunc {
	return BasicFuncAuth(func(user, pw string) bool {
		gotPw, ok := users[user]
		if !ok {
			return false
		}

		return pw == gotPw
	})
}

// BasicFuncAuth implements Basic (RFC 7617) Auth by passing the user and password to the given function which returns whether their authorized
func BasicFuncAuth(fn func(user, pw string) bool) AuthFunc {
	return func(c *gin.Context, scheme string, tok string) bool {
		if scheme != "basic" {
			return false
		}

		decTok, err := base64.StdEncoding.DecodeString(tok)
		if err != nil {
			return false
		}

		ps := strings.SplitN(string(decTok), ":", 2)
		if len(ps) != 2 {
			return false
		}

		return fn(ps[0], ps[1])
	}
}

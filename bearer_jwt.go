// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginauth

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/auth"
	"github.com/gin-gonic/gin"
)

// BearerJWTRoleAuth implements Bearer JWT (RFC 7519) Auth by verifying the token using the given JwtContext and checking whether the parsed token contains the given role
func BearerJWTRoleAuth(jwtCtx *auth.JwtContext, clientID string, role string) AuthFunc {
	return BearerJWTRolesFuncAuth(jwtCtx, clientID, func(tok *auth.KeycloakToken, roles []string) bool {
		for _, r := range roles {
			if r == role {
				return true
			}
		}

		return false
	})
}

// BearerJWTRolesFuncAuth implements Bearer JWT (RFC 7519) Auth by verifying the token using the given JwtContext and passing the parsed token and roles to the given function which returns whether it's authorized
func BearerJWTRolesFuncAuth(jwtCtx *auth.JwtContext, clientID string, fn func(tok *auth.KeycloakToken, roles []string) bool) AuthFunc {
	return BearerJWTFuncAuth(jwtCtx, clientID, func(tok *auth.KeycloakToken) bool {
		roles, err := tok.ClientRoles(clientID)
		if err != nil {
			return false
		}

		return fn(tok, roles)
	})
}

// BearerJWTFuncAuth implements Bearer JWT (RFC 7519) Auth by verifying the token using the given JwtContext and passing the parsed token to the given function which returns whether it's authorized
func BearerJWTFuncAuth(jwtCtx *auth.JwtContext, clientID string, fn func(tok *auth.KeycloakToken) bool) AuthFunc {
	return func(c *gin.Context, scheme string, tok string) bool {
		if scheme != "bearer" {
			return false
		}

		token, err := jwtCtx.VerifyToken(c, tok, clientID)
		if err != nil {
			return false
		}

		return fn(token)
	}
}
